This is a module of STL-ARIMA, a model for time series forecasting based on STL decomposition and ARIMA.

The decomposed time series data at time $t$ can be written as

$$ y_t=s_t+a_t,$$

where $s_t$ is the seasonal component and $a_t$ is the seasonally adjusted component.
The seasonally adjusted component itself is actually $a_t=t_t+r_t$, where $t_t$ and $r_t$ are the trend and residual components, respectively.

Different from usual ARIMA model that forecasts directly on the original time series $y_t$, STL-ARIMA performs forecasting on both seasonal component $s_t$ and seasonally-adjusted component $a_t$ independently. 
The forecasting result of both components are then added up to obtain the final result.

## Usage example

Only using endogenous feature:
```python
from stl_arima import STLARIMA

# Initialize STLARIMA model having 14 lags to use as predictors
model = STLARIMA(lags=14)
model.fit(endog=y_train)

# Perform forecasting for the next 7 data points
pred = model.forecast(steps=7)
```

Including exogenous features:
```python
model = STLARIMA(lags=14)
model.fit(endog=y_train, exog=X_train)

# Perform forecasting for the next 7 data points.
# Make sure the size of exog's first dimension equals
# to the number of steps.
pred = model.forecast(steps=7, exog=X_test)
```

