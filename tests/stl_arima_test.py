from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
from stl_arima import STLARIMA


def create_data_splits():
    X, y = load_boston(return_X_y=True)
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42
    )
    return X_train, X_test, y_train, y_test


def test_fit():
    X_train, X_test, y_train, y_test = create_data_splits()
    model = STLARIMA(lags=7)
    model.fit(endog=y_train, exog=X_train)
    assert model._is_fitted

    pred = model.forecast(exog=X_test, steps=X_test.shape[0])
    assert pred.shape == y_test.shape


def test_endog_only():
    _, _, y_train, y_test = create_data_splits()
    model = STLARIMA(lags=7)
    model.fit(endog=y_train)
    assert model._is_fitted

    pred = model.forecast(steps=y_test.shape[0])
    assert pred.shape == y_test.shape
