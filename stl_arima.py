""" This module contains the class for the STL-ARIMA model.

"""

from typing import Any, Optional

import numpy as np
from statsmodels.tsa.arima.model import ARIMA
from statsmodels.tsa.seasonal import STL


class STLARIMA:
    """A STL-ARIMA model class.

    Different from the regular ARIMA model, we perform STL decomposition on the
    time series and forecast "seasonal" component and "seasonally adjusted" component
    independently. The forecasted seasonal component and seasonally adjusted
    component are added to get the final forecast.

    Attributes
    ----------
    lags : int
        The number of lags in the STL decomposition.
    d : int
        The degree of differencing in the ARIMA model.
    q : int
        The order of the seasonal ARIMA model.
    exog_on_seasonal : bool
        Whether to use exogenous data for seasonal component forecasting.

    Methods
    ----------
    fit(endog, exog=None)
        Fit the model. It also accepts exogenous features.
    forecast(exog=None, steps=1)
        Perform forecasting with the fitted model. By default, it only does 
        one-step forecasting. Multistep forecasts can be obtained by specifying
        `steps` value. When the model is fitted using exogenous features, the 
        argument for `exog` must also be specified.

    Notes
    -----
    The 'p' parameter of the ARIMA model always equals to the number of lags
    in the STL decomposition.

    """

    def __init__(
        self, lags: int, d: int = 1, q: int = 1, exog_on_seasonal: bool = False,
    ):
        self.lags = lags
        self.d = d
        self.q = q
        self.exog_on_seasonal = exog_on_seasonal

        self.stl: STL = None
        self.seasonal_estimator: Any = None
        self.seasonally_adjusted_estimator: Any = None
        self.seasonal_estimator_fitted: Any = None
        self.seasonally_adjusted_estimator_fitted: Any = None
        self._is_fitted = False

    def fit(self, endog: np.ndarray, exog: Optional[np.ndarray] = None) -> "STLARIMA":
        """Fit the model.

        Parameters
        ----------
        endog : np.ndarray
            The time series to be fitted. This must be squeezable to a 1-D array.
        exog : np.ndarray, optional
            The exogenous features.

        """

        # First we need to perform STL decomposition to get seasonal component
        # and seasonally adjusted component.
        self.stl = STL(endog, period=self.lags)
        stl_result = self.stl.fit()

        seasonal = stl_result.seasonal
        seasonally_adjusted = stl_result.trend + stl_result.resid

        # Check if exogenous data is needed to forecast seasonal component.
        x_seasonal = exog if self.exog_on_seasonal else None

        # Independently fit the seasonal component and seasonally adjusted component.
        self.seasonal_estimator = ARIMA(
            seasonal, order=(self.lags, 1, self.q), exog=x_seasonal
        )
        self.seasonally_adjusted_estimator = ARIMA(
            seasonally_adjusted, order=(self.lags, 1, self.q), exog=exog
        )

        self.seasonal_estimator_fitted = self.seasonal_estimator.fit()
        self.seasonally_adjusted_estimator_fitted = (
            self.seasonally_adjusted_estimator.fit()
        )

        self._is_fitted = True
        return self

    def forecast(self, exog: Optional[np.ndarray] = None, steps: int = 1) -> np.ndarray:
        """Perform forecasting with the fitted model.

        Parameters
        ----------
        exog : np.ndarray, optional
            The exogenous features.
        steps : int, optional
            The number of steps to forecast.

        """
        if not self._is_fitted:
            raise Exception("The model is not fitted yet. Please run fit() first.")

        # Check if exogenous data for seasonal is used
        x_seasonal = exog if self.exog_on_seasonal else None

        pred_seasonal = self.seasonal_estimator_fitted.forecast(
            steps=steps, exog=x_seasonal
        )
        pred_seasonally_adjusted = self.seasonally_adjusted_estimator_fitted.forecast(
            steps=steps, exog=exog
        )

        final_pred = pred_seasonal + pred_seasonally_adjusted
        return final_pred
