from setuptools import setup

with open("README.md") as readme_file:
    readme = readme_file.read()

setup(
    name="stl_arima",
    py_modules=["stl_arima"],
    version="0.1.0",
    description="STL-ARIMA model for time series forecasting",
    author="Aria Ghora Prabono",
    author_email="aria.ghora@nri.co.id",
    long_description=readme,
    long_description_content_type="text/markdown; charset=UTF-8",
)
